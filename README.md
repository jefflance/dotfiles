# My dotfiles

My dotfiles for awesome, emacs, vim, zsh, tmux,...

## Externals / Librairies used
  * [dotbot](https://github.com/anishathalye/dotbot/)
  * [editorconfig](https://editorconfig.org/)
  * [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
  * [vim-plug](https://github.com/junegunn/vim-plug)
