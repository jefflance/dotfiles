set nocompatible              " be iMproved, required
filetype off                  " required



""""""""""""""""""""""""""""""""""""""""""""""""""""""
"  ___ _   _ _____ _____ ____  _____ _    ____ _____
" |_ _| \ | |_   _| ____|  _ \|  ___/ \  / ___| ____|
"  | ||  \| | | | |  _| | |_) | |_ / _ \| |   |  _|
"  | || |\  | | | | |___|  _ <|  _/ ___ \ |___| |___
" |___|_| \_| |_| |_____|_| \_\_|/_/   \_\____|_____|
"
""""""""""""""""""""""""""""""""""""""""""""""""""""""
" UTF-8
set encoding=utf8

"syntax highlighting
syntax on
set synmaxcol=512
filetype off

" stop unnecessary rendering
set lazyredraw

" show line numbers
set number

" no line wrapping
set nowrap

" Allow saving of files as sudo when I forgot to start vim using sudo.
cnoremap w!! w !sudo tee > /dev/null %

" Start in insert mode for ALL files
"au BufRead,BufNewFile * startinsert
"startinsert



"""""""""""""""""""""""""""""""""""""""""""
"  ____  _    _   _  ____ ___ _   _ ____   
" |  _ \| |  | | | |/ ___|_ _| \ | / ___|  
" | |_) | |  | | | | |  _ | ||  \| \___ \  
" |  __/| |__| |_| | |_| || || |\  |___) | 
" |_|   |_____\___/ \____|___|_| \_|____/
" 
"""""""""""""""""""""""""""""""""""""""""""
" Installing the Plug plugin manager
source $HOME/.vim/plug.vim

" start NERDTree if no file is pass as arg
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" close vim if the only window left open is a NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"  ____ _____  _  _____ _   _ ____  _     ___ _   _ _____
" / ___|_   _|/ \|_   _| | | / ___|| |   |_ _| \ | | ____|
" \___ \ | | / _ \ | | | | | \___ \| |    | ||  \| |  _|
"  ___) || |/ ___ \| | | |_| |___) | |___ | || |\  | |___
" |____/ |_/_/   \_\_|  \___/|____/|_____|___|_| \_|_____|
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set noshowmode
set laststatus=2
if !has('gui_running')
  set t_Co=256
endif

let g:lightline = {
      \ 'colorscheme': 'jellybeans',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component': {
      \   'readonly': '%{&readonly?"⭤":""}',
      \ },
      \ 'separator': { 'left': '⮀', 'right': '⮂' },
      \ 'subseparator': { 'left': '⮁', 'right': '⮃' }
  \ }



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"   ____ ___  _     ___  ____  ____   ____ _   _ _____ __  __ _____
"  / ___/ _ \| |   / _ \|  _ \/ ___| / ___| | | | ____|  \/  | ____|
" | |  | | | | |  | | | | |_) \___ \| |   | |_| |  _| | |\/| |  _|
" | |__| |_| | |__| |_| |  _ < ___) | |___|  _  | |___| |  | | |___
"  \____\___/|_____\___/|_| \_\____/ \____|_| |_|_____|_|  |_|_____|
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
colorscheme dracula



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"        _             _                      _   _   _
"  _ __ | |_   _  __ _(_)_ __  ___   ___  ___| |_| |_(_)_ __   __ _ ___
" | '_ \| | | | |/ _` | | '_ \/ __| / __|/ _ \ __| __| | '_ \ / _` / __|
" | |_) | | |_| | (_| | | | | \__ \ \__ \  __/ |_| |_| | | | | (_| \__ \
" | .__/|_|\__,_|\__, |_|_| |_|___/ |___/\___|\__|\__|_|_| |_|\__, |___/
" |_|            |___/                                        |___/
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"
" Markdown review
"
let vim_markdown_preview_toggle=1
let vim_markdown_preview_toggle=2
let vim_markdown_preview_browser='chromium-browser'

"
" UltiSnips
"
if has("python")
    let g:UltiSnipsUsePythonVersion = 2
elseif has("python3")
    let g:UltiSnipsUsePythonVersion = 3
endif



""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"  _  _________   ______ ___ _   _ ____ ___ _   _  ____ ____
" | |/ / ____\ \ / / __ )_ _| \ | |  _ \_ _| \ | |/ ___/ ___|
" | ' /|  _|  \ V /|  _ \| ||  \| | | | | ||  \| | |  _\___ \
" | . \| |___  | | | |_) | || |\  | |_| | || |\  | |_| |___) | 
" |_|\_\_____| |_| |____/___|_| \_|____/___|_| \_|\____|____/
" 
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Load keybindings from file
source $HOME/.vim/keybindings.vim
